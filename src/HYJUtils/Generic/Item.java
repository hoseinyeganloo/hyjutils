package HYJUtils.Generic;

public class Item<T>{
	
	private T _Value = null;
	
	public void setValue(T e){
		_Value = e;
	}
	
	public T getValue(){
		return _Value;
	}
	
	public Item(T value){
		_Value = value;
	}
	
	public Item<T> Next;
	public Item<T> Previous;
}	
