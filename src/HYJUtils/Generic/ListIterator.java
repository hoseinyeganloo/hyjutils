package HYJUtils.Generic;

import java.util.EmptyStackException;

public class ListIterator<T> implements java.util.ListIterator<T> {
	
	private Item<T> _Head = new Item<T>(null);
	private Item<T> _Tail = new Item<T>(null);
	private Item<T> _Current;
	private int _index = 0;
	private int _Size = 0;
	
	public int getSize(){
		return _Size;
	}
	
	public void ResetIndex(){
		_index = 0;
		_Current = _Head.Next;
	}
	
	@Override
 	public boolean hasNext() {
		return _index < _Size;
	}

	@Override
	public T next() {
		_Current = _Current.Next;
		_index++;
		return _Current.getValue();
	}

	@Override
	public boolean hasPrevious() {
		return _Current.Previous != _Head;
	}

	@Override
	public T previous() {
		_Current = _Current.Previous;
		_index--;
		return _Current.getValue();
	}

	@Override
	public int nextIndex() {
		return _index+1;
	}

	@Override
	public int previousIndex() {
		return _index-1;
	}

	@Override
	public void remove() {
		if(_Size == 0)
			throw new EmptyStackException();
		Item<T> tmp = _Current.Next;
		_Size--;
		_Current.Previous.Next = _Current.Next;
		_Current.Next.Previous = _Current.Previous;
		_Current.setValue(null);
		_Current.Next = null;
		_Current.Previous = null;
		_Current = tmp;
	}

	@Override
	public void set(T e) {
		_Current.setValue(e);
	}

	@Override
	public void add(T e) {
		_Size++;
		Item<T> tmp = new Item<T>(e);
		tmp.Next = _Tail;
		_Tail.Previous.Next = tmp;
		_Tail.Previous = tmp;		
	}

}
